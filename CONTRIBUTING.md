Contributing
============

Thank you for your interest in contributing to this project.

Submitting a patch
------------------

1. It's generally best to start by opening a new issue describing the bug or
   feature you're intending to fix.  Even if you think it's relatively minor,
   it's helpful to know what people are working on.  Mention in the initial
   issue that you are planning to work on that bug or feature so that it can
   be assigned to you.

2. Follow the normal process of [forking] the project, and setup a new
   branch to work in.  It's important that each group of changes be done in
   separate branches in order to ensure that a pull request only includes the
   commits related to that bug or feature.

3. Do your best to have [well-formed commit messages] for each change.
   This provides consistency throughout the project, and ensures that commit
   messages are able to be formatted properly by various git tools.

4. Finally, push the commits from your fork and submit a [merge request].

[forking]: https://docs.gitlab.com/ce/gitlab-basics/fork-project.html

[well-formed commit messages]: http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html
[merge request]: https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html
