ANSIBLE_NODE
===============

Provision an Node to manage be with Ansible.

* Install python with simplejson module
* Debian distros install python apt module
* RedHat distros install python selinux module

Requirements
------------

Ansible 2.4+

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
      - role: ansible_role

License
-------

GPLv3

Author Information
------------------

- Robert Brightling | [email](mailto:dev@robert.brightling.me.uk) | [GitLab](https://gitlab.com/brightling) | [GitHub](https://github.com/brightlingmeuk) | [Galaxy](https://galaxy.ansible.com/brightlingmeuk)
